# Subject Mock API
This API is a mock of the swagger API called pet, there are some operations defined, see below for a more detailled description
## Build mock

```bash
$ docker build -t mock .
```

## Run mock

```bash
$ docker run --rm --name mock -d -p 8080:8080 mock
```

You can access the API to fetch all pets at `http://localhost:8080/pets`

If you want to access a specific pet we have 3 pets defined being: dog, cat and fish. 
To receive a status about the different pets you can access the API like `http://localhost:8080/pets/dog`

Inside the API logic we've also introduced some error handling, meaning if we search for a pet which is not known yet we're getting an Error back 
"Pet is not known in the store" eg. `http://localhost:8080/pets/shikorita`
