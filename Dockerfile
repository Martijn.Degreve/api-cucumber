FROM https://hub.docker.com/r/rodolpheche/wiremock

COPY stubs /home/wiremock
COPY pet-response.json /home/wiremock/__files/
COPY dog.json /home/wiremock/__files/
COPY cat.json /home/wiremock/__files/
COPY fish.json /home/wiremock/__files/