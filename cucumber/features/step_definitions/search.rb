require 'net/http'
require 'json'

Given("petstore search API is up and running") do
  $url="http://localhost:8080/pets"
end

When("i perform a search for list of pets") do
  $uri=URI($url)
  $response=JSON.parse(Net::HTTP.get($uri))
  #puts $response
end

Then("i should get list of pets with status") do
  expect($response[0]["name"]).to eq "dog"
  expect($response[0]["status"]).to eq "available"
end

Then("i should get list of pets with status:") do |table|
  $expectedResponseTable = table.raw
  expect($response[0]["name"]).to eq $expectedResponseTable[1][0]
  expect($response[0]["status"]).to eq $expectedResponseTable[1][1]
  expect($response[1]["name"]).to eq $expectedResponseTable[2][0]
  expect($response[1]["status"]).to eq $expectedResponseTable[2][1]
  expect($response[2]["name"]).to eq $expectedResponseTable[3][0]
  expect($response[2]["status"]).to eq $expectedResponseTable[3][1]

end