Feature: Pet search

  Scenario: Search for availability status list of pets
    Given petstore search API is up and running
    When i perform a search for list of pets
    Then  i should get list of pets with status:
      | name | status        |
      | dog  | available     |
      | cat  | available     |
      | fish | not available |
